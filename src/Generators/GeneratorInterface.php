<?php
namespace Magicsquare\Laragen\Generators;

interface GeneratorInterface
{
    public function generate();
}
