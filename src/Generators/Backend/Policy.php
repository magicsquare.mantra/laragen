<?php

namespace Magicsquare\Laragen\Generators\Backend;

use Magicsquare\Laragen\Models\LaragenOptions;
use Magicsquare\Laragen\Generators\BaseGenerator;
use Magicsquare\Laragen\Generators\GeneratorInterface;

class Policy extends BaseGenerator implements GeneratorInterface
{
    protected static $initializeFlag = 0;

    protected $namespace  = "App\Policies";

    public function generate()
    {
        $generatedFiles = [];

        if ($this::$initializeFlag == 0) {
            $laragen = LaragenOptions::getInstance();
            $modules = $laragen->getModules();
            $models = [];
            foreach ($modules as $module) {
                $models[] = $module->getModelName();
            }

            $modelsCode = '';
            foreach ($models as $model) {
                $modelsCode .= "'App\Models\\" . $model . "' => 'App\Policies\\" . $model . "Policy'," . PHP_EOL . $this->getTabs(2);
            }

            $policyProviderTemplate = $this->buildTemplate('common/LaragenPolicyServiceProvider', [
                '{{bindPolicies}}'     => $modelsCode,
            ]);

            $fullFilePath = $this->getPath("app/Providers/") . "LaragenPolicyServiceProvider.php";
            file_put_contents($fullFilePath, $policyProviderTemplate);
            $generatedFiles[] = $fullFilePath;
            $this::$initializeFlag++;
        }

        $controllerTemplate = $this->buildTemplate('backend/policies/policy', [
            '{{modelName}}'           => $this->module->getModelName(),
            '{{namespace}}'          => $this->namespace,
            '{{modelNameLowercase}}' => $this->module->getModelNamePluralLowercase(),
        ]);

        $fullFilePath = $this->getPath("app/Policies/") . $this->module->getModelName() . "Policy" . ".php";
        file_put_contents($fullFilePath, $controllerTemplate);
        $generatedFiles[] = $fullFilePath;
        return $generatedFiles;
    }
}
