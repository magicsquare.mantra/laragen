<?php

namespace Magicsquare\Laragen\Generators\Backend;

use Illuminate\Support\Str;
use Magicsquare\Laragen\Generators\BaseGenerator;

use Magicsquare\Laragen\Generators\GeneratorInterface;

class Notification extends BaseGenerator implements GeneratorInterface
{
    protected $destination = "laragen/app/Notifications";
    protected $namespace  = "Laragen\App\Notifications";
    protected $template  = "backend/notifications/notification";
    protected $fileSuffix  = "Notification";

    protected $childDestination = "app/Notifications/Backend";
    protected $childNamespace  = "App\Notifications\Backend";


    public function generate()
    {
        $fullFilePaths = [];
        $eventsType = config('laragen.options.events');

        foreach ($eventsType as $eventType) {
            $notificationTemplate = $this->buildTemplate($this->template, [
                '{{namespace}}'          => $this->namespace,
                '{{modelName}}'          => $this->module->getModelName(),
                '{{eventTypeUppercase}}'          => Str::ucfirst($eventType),
                '{{eventTypeLowercase}}'          => Str::lower($eventType)
            ]);

            return  $this->generateFile($notificationTemplate);

            // $fullFilePath = $this->getPath($this->destination . "/") . $this->module->getModelName() . Str::ucfirst($eventType) . "Notification" . ".php";
            // $fullFilePaths[] = $fullFilePath;
            // file_put_contents($fullFilePath, $controllerTemplate);
        }

        return $fullFilePaths;
    }
}
