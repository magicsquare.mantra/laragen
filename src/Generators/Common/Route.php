<?php

namespace Magicsquare\Laragen\Generators\Common;

use Magicsquare\Laragen\Models\LaragenOptions;
use Magicsquare\Laragen\Generators\BaseGenerator;
use Magicsquare\Laragen\Generators\GeneratorInterface;

class Route extends BaseGenerator implements GeneratorInterface
{
    protected static $initializeFlag = 0;

    private static $destination = "routes";

    public function generate()
    {
        $generatedFiles = [];

        $backendAuthRouteFile = $this->getPath(self::$destination . "/backend//") . "auth.php";
        $webRouteFile = $this->getPath(self::$destination . "/frontend//") . "web.php";
        $apiRouteFile = $this->getPath(self::$destination . "/api//") . "api.php";
        $backendWebRouteFile = $this->getPath(self::$destination . "/backend//") . "web.php";

        if (self::$initializeFlag++ == 0) {
            $this->initializeFiles([
                $backendAuthRouteFile => "backend/routes/auth",
                $apiRouteFile => "common/routes/api",
                $backendWebRouteFile => "backend/routes/web"
            ]);
        }

        $laragen = LaragenOptions::getInstance();
        if ($laragen->generatorExists('Frontend\\ApiController')) {
            $this->insertIntoFile(
                $webRouteFile,
                "<?php\n",
                "use App\\Http\\Controllers\\" . $this->module->getModelName() . "Controller;\n"
            );

            $this->insertIntoFile(
                $webRouteFile,
                "/" . "* Insert your routes here */",
                "\n" . $this->getTabs(1) . "Route::resource('" . $this->module->getModuleName() . "', " . $this->module->getModelName() . "Controller::class);"
            );
            $generatedFiles[] = $webRouteFile;
        }

        if ($laragen->generatorExists('Backend\\Controller')) {
            $this->insertIntoFile(
                $backendWebRouteFile,
                "<?php\n",
                "use App\\Http\\Controllers\\Backend\\" . $this->module->getModelName() . "Controller;\n"
            );

            $this->insertIntoFile(
                $backendWebRouteFile,
                "/" . "* Insert your routes here */",
                "\n" . $this->getTabs(1) . "Route::resource('" . $this->module->getModuleName() . "', " . $this->module->getModelName() . "Controller::class);"
            );
            $generatedFiles[] = $backendWebRouteFile;
        }

        if ($laragen->generatorExists('Common\\ApiController')) {

            $this->insertIntoFile(
                $apiRouteFile,
                "/" . "* Insert public api routes here */",
                "\n" . $this->getTabs(1) . "Route::apiResource('" . $this->module->getModuleName() . "', '" . $this->module->getModelName() . "ApiController')" . "->only(['index', 'show'])" . ";"
            );


            $this->insertIntoFile(
                $apiRouteFile,
                "/" . "* Insert private api routes here */",
                "\n" . $this->getTabs(1) . "Route::apiResource('" . $this->module->getModuleName() . "', '" . $this->module->getModelName() . "ApiController')" . "->only(['store', 'update', 'destroy'])" . ";"
            );
        }

        return [];
    }
}
