<?php

namespace {{namespace}};

use App\User;
use App\Models\{{modelName}};
use Illuminate\Auth\Access\HandlesAuthorization;

class {{modelName}}Policy
{
    public function view(User $user, {{modelName}} ${{modelNameLowercase}})
    {
    	if ($user === null) {
            return false;
        }

        // admin overrides
        if ($user->can('api-view-{{modelNameLowercase}}') || $user->can('view-{{modelNameLowercase}}')) {
            return true;
        }

        return $user->id == ${{modelNameLowercase}}->user_id;
    }

    public function create(User $user, {{modelName}} ${{modelNameLowercase}})
    {
    	if ($user->can('api-create-{{modelNameLowercase}}') || $user->can('create-{{modelNameLowercase}}')) {
            return true;
        }
    }

    public function update(User $user, {{modelName}} ${{modelNameLowercase}})
    {
    	if ($user->can('api-edit-{{modelNameLowercase}}') || $user->can('edit-{{modelNameLowercase}}')) {
            return true;
            // return $user->id == ${{modelNameLowercase}}->user_id;
        }

        if ($user->can('api-crud-{{modelNameLowercase}}') || $user->can('crud-{{modelNameLowercase}}')) {
            return true;
        }
    }

    public function delete(User $user, {{modelName}} ${{modelNameLowercase}})
    {
    	if ($user->can('api-delete-{{modelNameLowercase}}') || $user->can('delete-{{modelNameLowercase}}')) {
            return $user->id == ${{modelNameLowercase}}->user_id;
        }

        if ($user->can('api-crud-{{modelNameLowercase}}')) {
            return true;
        }
    }
}