<?php
namespace Magicsquare\Laragen\Models\Types;

class GeneralType extends LaragenType
{
    protected $general = true;
    protected $isRelational = false;
}
