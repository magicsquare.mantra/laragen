<?php
namespace Magicsquare\Laragen\Models\Types\General;
use Magicsquare\Laragen\Models\Types\GeneralType;

class IntegerType extends GeneralType
{
    protected $dataType = 'integer';
    protected $formType = 'integer';
    protected $size = false;
}
