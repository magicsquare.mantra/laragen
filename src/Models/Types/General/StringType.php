<?php
namespace Magicsquare\Laragen\Models\Types\General;
use Magicsquare\Laragen\Models\Types\GeneralType;

class StringType extends GeneralType
{
    protected $dataType = 'string';
    protected $formType = 'string';
}
