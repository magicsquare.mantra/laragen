<?php
namespace Magicsquare\Laragen\Models\Types\General;
use Magicsquare\Laragen\Models\Types\GeneralType;

class BooleanType extends GeneralType
{
    protected $dataType = 'boolean';
    protected $formType = 'boolean';
}
