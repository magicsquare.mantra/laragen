<?php
namespace Magicsquare\Laragen\Models\Types;

class RelationalType extends LaragenType
{
    protected $isRelational = true;
}
