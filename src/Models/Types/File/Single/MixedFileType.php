<?php
namespace Magicsquare\Laragen\Models\Types\File\Single;
use Magicsquare\Laragen\Models\Types\File\SingleType;

class MixedFileType extends SingleType
{
    protected $hasFile = true;
    protected $extensions = '.zip,.doc,.txt,.pdf,.csv';
    protected $formType = 'file';
}
