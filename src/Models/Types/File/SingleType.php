<?php
namespace Magicsquare\Laragen\Models\Types\File;
use Magicsquare\Laragen\Models\Types\FileType;

class SingleType extends FileType
{
    protected $dataType = 'string';
}
