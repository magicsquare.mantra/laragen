# Laragen: Extensive generators for laravel

This is a work in progress. Please check back later for complete package.

## Features
Generate and scaffold everything required for a basic laravel project. Starting with controllers, models, requests, views, assets, migrations, seeders, routes. More to be added in future releases.

## Install

Via Composer

``` bash
$ composer require magicsquare/laragen
```

## Usage

To be updated later.

``` bash
$ php artisan laragen:make
```

## Security

If you discover any security related issues, please email pratiek.karki@gmail.com instead of using the issue tracker.

## Credits

- [Prateek Karki](http://prateekkarki.com.np)
- [Vikal Stha](http://ingnepal.org.np)
- [Anish Poudel](http://ingnepal.org.np)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

