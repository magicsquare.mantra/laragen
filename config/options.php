<?php
return [
    'items_to_generate' => [
        'Common' => [
            'Migration',
            'Model',
            'Seeder',
            'Route',
            'Resource',
            'JsonResourceCollection',
            'ApiController',
        ],
        'Backend' => [
            'Controller',
            // 'Api',
            'Request',
            'View',
            'Policy',
            'Observer'
        ]
    ],
    'image_sizes' => [
        'sm' => '850x850',
        'md' => '1350x1350',
        'xs' => '350x350',
    ],
    'events'    => [
        'created', 'updated', 'deleted'
    ],
    'seed_rows' => 15,
    'listing_per_page' => 20,
    'generic_fields' => true,
    'seo_fields' => true,
    'user_model' => 'App\User',
    'theme' => [
        'backend' => 'dark', // light or dark
        'frontend' => '', // theme folder
    ],
];
