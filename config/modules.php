<?php

// Data options separated by '|'
// Arguments for options separated by ':'

// Regular data types: 
//      string, int, text, bool, date

// Data type options 
//      unique, required

//Must start with data type nd then followed by size, then by modifiers if required

// Special data types: 
//      parent: requires name of a module, creates a one-to-many relation with the current module
//      related: requires name of a module, creates many to many relation with current module

return [

    'locations' => [
        'structure' => [
            'title'             => 'string|max:128',
            'slug'              => 'string|max:128|unique|required',
            'image'             => 'image',
            'description'       => 'string'
        ]
    ],

    'sub_locations'     => [
        'title'             => 'string|max:128',
        'slug'              => 'string|max:128|unique|required',
        'image'             => 'image',
        'description'       => 'text',
        'short_code'        => 'string',
        'location'          => 'parent:locations'
    ],
    
    'schools'     => [
        'name'                     => 'string|max:128',
        'short_code'               => 'string',
        'logo'                     => 'image',
        'contact_person'           => 'string|max:128',
        'contact_person_post'      => 'string|max:128',
        'contact_person_phone'     => 'string|max:128',
        'phone_number'             => 'string|max:128',
        'registered_date'          => 'datetime',
        'remarks'                  => 'string',
        'is_active'                => 'boolean',
        'lat'                      => 'integer',
        'lng'                      => 'integer',
        'sub_location'             => 'parent:sub_locations'
    ],

    'employees' => [
        'gender'            => 'options|Male:Female',
        'phone'             => 'string|max:256',
        'mobile'            => 'string|max:256',
        'associated_user'   => 'parent:users',
        'permanent_address' => 'string|max:512',
        'temporary_address' => 'string|max:512',
        'date_of_birth'     => 'date',
        'description'       => 'text',
        'position'          => 'options|Educator Manager:Teacher:Assistant Teacher',
        'date_joined'       => 'date',
        'image'             => 'image',
        'citizenship'       => 'image',
        'pan_no'            => 'integer',
        'salary'            => 'integer',
        'ing_id'            => 'string',
        'is_active'         => 'boolean',
    ],

    'attendances'     => [
        'employee'          => 'parent:users',
        'date_of_birth'     => 'date',
        'is_present'        => 'boolean',
        'lat'               => 'integer',
        'lng'               => 'integer',

    ],
    
    'course_tags'     => [
        'title'             => 'string|max:128',
        'slug'              => 'string|max:128|unique|required',
        'description'       => 'text',
    ],

    'course_groups'     => [
        'title'             => 'string|max:128',
        'slug'              => 'string|max:128|unique|required',
        'image'             => 'image',
        'description'       => 'text',
    ],

    'course_titles' => [
        'structure' => [
            'title'             => 'string|max:128',
            'slug'              => 'string|max:128|unique|required',
            'image'             => 'gallery',
            'short_description' => 'text',
            'description'       => 'text',
            'category'          => 'parent:course_groups',
            'tags'              => 'related:course_tags'
        ]
    ],

    'courses' => [
        'structure' => [
            'title'             => 'string|max:128',
            'slug'              => 'string|max:128|unique|required',
            'image'             => 'gallery',
            'short_description' => 'text',
            'description'       => 'text',
            'category'          => 'parent:course_titles',
        ]
    ],

    'course_stats' => [
        'structure' => [
            'course'            => 'parent:courses',
            'description'       => 'text',
            'remarks'           => 'text',
            'date'              => 'date',
        ]
    ],

    'cms_pages' => [
        'structure' => [
            'title'             => 'string|max:128',
            'slug'              => 'string|max:128|unique|required',
            'gallery'           => 'gallery',
            'image'             => 'image',
            'content'           => 'text',
            'employee'          => 'parent:users',
        ]
    ],
        
    'testimonials'     => [
        'title'                 => 'string|max:128',
        'image'                 => 'image',
        'description'           => 'text',
        'reviewer'              => 'string',
        'reviewer_address'      => 'string',
        'reviewer_company'      => 'string',
        'reviewer_post'         => 'string'
    ],

    'video_groups' => [
        'structure' => [
            'title'             => 'string|max:128',
            'slug'              => 'string|max:128|unique|required',
        ]
    ],

    'videos' => [
        'structure' => [
            'title'             => 'string|max:128',
            'description'       => 'text',
            'youtube_link'      => 'string|max:192',
            'group'             => 'parent:video_groups',
        ]
    ],

    'downloads' => [
        'structure' => [
            'title'             => 'string|max:128',
            'description'       => 'text',
            'link'              => 'file',
        ]
    ],

    'faq_groups' => [
        'structure' => [
            'title'             => 'string|max:128',
            'slug'              => 'string|max:128|unique|required',
        ]
    ],

    'faqs' => [
        'structure' => [
            'title'             => 'string|max:128',
            'description'       => 'text',
            'group'          => 'parent:faq_groups'
        ]
    ],
    
    'sliders' => [
        'structure' => [
            'title'             => 'string|max:128',
            'slug'              => 'string|max:128|unique|required',
            'image'             => 'image',
            'content'           => 'string',
            'sub_content'       => 'string',
            'link_1'            => 'string',
            'link_2'            => 'string'
        ]
    ],

    'business_settings' => [
        'structure' => [
            'business_name'    => 'string|max:128',
            'business_intro'   => 'text',
            'business_logo'    => 'image',
            'address'          => 'string|max:250',
            'email'            => 'string|max:192',
            'mobile'           => 'string|max:192',
            'phone'            => 'string|max:192',
            'map_link'         => 'string',
            'fb_link'          => 'string|max:192',
            'insta_link'       => 'string|max:192',
            'youtube_link'     => 'string|max:192',
            'wechat_link'      => 'string|max:192',
            'viber_link'       => 'string|max:192',
            'whatsapp_link'    => 'string|max:192',
            'skype_link'       => 'string|max:192',
        ]
    ]
];
